import React, { Component } from 'react';
import './App.css';
import CaptureEmailForm from './CaptureEmailForm';
import Instagram from './Instagram'

class App extends Component {
  render() {
    const asciiBike = "    __o\n  _`\\<,_\n (*)/ (*)";

    return (
      <div>
        <div className="outer">
          <h1>BIKE THE 101!</h1>
          <h3>Launching Soon on Web, Android and iOS</h3>
        </div>
        <pre>{asciiBike}</pre>
        <div className="outer">
          <h2>
            A bicycle touring community that shares the best routes,
            camping, cafes, and beaches of the British Columbia Sunshine
            Coast.
          </h2>
          <h2>
            From Langdale to Lund, get the most out of your tour.
          </h2>
          <h2>
            Sign up and be the first to know when the app launches!
          </h2>
          <CaptureEmailForm />

          <br/>
          <div style={{textAlign: 'center'}}>
            <Instagram />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
