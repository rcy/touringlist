import React from 'react';

const Instagram = () => (
  <a className="instagram" href="https://instagram.com/bike_the_101">
    Find us on Instagram <i className="fa fa-large fa-instagram"></i>
  </a>
)
export default Instagram;
