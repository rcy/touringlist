import React from 'react'
import SubscribeFrom from 'react-mailchimp-subscribe'

const CaptureEmailForm = () => {
  const formProps = {
    action: '//bikethe101.us17.list-manage.com/subscribe/post?u=3c9e20749b630db57a072311f&amp;id=6c2328ef4e',
    messages: {
      inputPlaceholder: "Your email",
      btnLabel: "Join",
      sending: "Sending...",
      success: "Thank you for your interest!",
      error: "Oops, cannot subscribe this address, try again"
    },
    styles: {
      sending: {
        fontSize: 18,
        color: "auto",
        background: "black"
      },
      success: {
        fontSize: 18,
        color: "green",
        background: "black"
      },
      error: {
        fontSize: 18,
        color: "red",
        background: "black"
      }
    }
  }
  return <SubscribeFrom {...formProps}/>
}

export default CaptureEmailForm
