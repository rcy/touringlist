export default [
  {
    path: '/landing',
    component: () => import('layouts/landing'),
    children: [
      { path: '', component: () => import('pages/landing') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/app'),
    children: [
      { path: 'places', name: 'places', component: () => import('pages/index') },
      { path: 'places/add', name: 'addArea', component: () => import('pages/AddArea') },
      { path: 'places/:id', name: 'area', component: () => import('pages/Area') },
      { path: 'places/:id/edit', name: 'editArea', component: () => import('pages/EditArea') },
      { path: 'links', name: 'links', component: () => import('pages/links') },
      { path: 'faves', name: 'faves', component: () => import('pages/links') },
      { path: 'user', name: 'user', component: () => import('pages/links') }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
