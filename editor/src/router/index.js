import Vue from 'vue'
import Router from 'vue-router'

import Table from '@/components/Table.vue'
import Admin from '@/components/admin/Admin.vue'
import PinDetail from '@/components/PinDetail.vue'
import PinEdit from '@/components/admin/PinEdit.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',

  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },

  routes: [
    {
      path: '/',
      component: Table,
    },
    {
      path: '/pin/:id',
      name: 'pin',
      component: PinDetail,
    },
    {
      path: '/pin/:id/edit',
      name: 'pinEdit',
      component: PinEdit,
    },
    {
      path: '/admin',
      component: Admin,
      children: [
        {
          path: 'pin/:id',
          name: 'adminPin',
          component: PinDetail,
        }
      ]
    },
  ]
})
