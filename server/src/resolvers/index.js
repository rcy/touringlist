const { Query } = require('./Query')
//const { Subscription } = require('./Subscription')
const { auth } = require('./Mutation/auth')
const { pin } = require('./Mutation/pin')
const { AuthPayload } = require('./AuthPayload')

module.exports = {
  Query,
  Mutation: {
    ...auth,
    ...pin,
  },
  //Subscription,
  AuthPayload,
}
