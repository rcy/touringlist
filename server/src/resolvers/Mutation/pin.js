const { forwardTo } = require('prisma-binding')

const pin = {
  updatePin: forwardTo('db'),
  createArea: forwardTo('db'),
  updateArea: forwardTo('db'),
}

module.exports = { pin }
