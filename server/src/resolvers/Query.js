const { forwardTo } = require('prisma-binding')
const { getUserId } = require('../utils')

const Query = {
  pin: forwardTo('db'),
  pins: forwardTo('db'),

  areas: forwardTo('db'),
  area: forwardTo('db'),

  me(parent, args, ctx, info) {
    const id = getUserId(ctx)
    return ctx.db.query.user({ where: { id } }, info)
  },
}

module.exports = { Query }
