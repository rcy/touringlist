const yelp = require('./yelp-scrape')
const ta = require('./ta-scrape')
const assert = require('assert')
const { request, GraphQLClient } = require('graphql-request')

const client = new GraphQLClient("http://localhost:4466/touringlist/dev", {
  headers: {
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InNlcnZpY2UiOiJ0b3VyaW5nbGlzdEBkZXYiLCJyb2xlcyI6WyJhZG1pbiJdfSwiaWF0IjoxNTIyNjkxMDQ3LCJleHAiOjE1MjMyOTU4NDd9.w6qD2X-f-w_venl5Ezjh3YcDOPV74UJ1NyWp3Jm3sh0"
  }
})

const store = async (data) => {
  assert.equal(data.vendor, 'yelp')

  // find existing doc associated with this data
  let id
  let doc = await client.request(`
    query FIND_PIN($url: String) {
      pins(where: { ${data.vendor}Url: $url }) { 
        id
        name
        phone
        address
        website
        city
      }
    }`, { url: data.url }).then(({ pins }) => pins[0])

  console.log({ doc, data })

  // setup a new doc
  if (!doc) {
    doc = {
      [data.vendor]: data,
      [data.vendor + 'Url']: data.url
    }
  } else {
    id = doc.id
    delete doc.id
  }

  // populate top level doc fields if they don't exist
  ;['name', 'phone', 'address', 'website'].forEach(f => {
    if (!doc[f]) {
      doc[f] = data[f] || null
    }
  })

  if (!doc.city) {
    doc.city = data.address.split(',')[1].trim()
  }

  const area = await client.request(`query ($name: String!) { area(where: { name: $name }) { id } }`, { name: doc.city })
                         .then(res => res.area)

  if (area) {
    doc.area = {
      connect: {
        id: area.id
      },
    }
  }

  if (id) {
    await client.request(`
      mutation UPDATE_PIN($id: ID!, $data: PinUpdateInput!) {
        updatePin(where: { id: $id }, data: $data) {
          id
        }
      }
    `, { id, data: doc })
  } else {
    await client.request(`
      mutation CREATE_PIN($data: PinCreateInput!) {
        createPin(data: $data) {
          id
        }
      }
    `, { data: doc })
  }
}


async function main() {
  const locs = [
    "Port Mellon, BC",
    "Langdale,+BC",
    "Gibsons, BC",
    "Roberts Creek, BC",
    "Wilson Creek, BC",
    "Sechelt, BC",
    "Madeira Park, BC",
    "Pender Harbour, BC",
    "Egmont, BC",
    "Powell River, BC",
    "Lund, BC",
  ]

  const descs = [
    "Accommodations",
    "Bike Shops",
    "Campgrounds",
    "Coffee",
    "Convenience Stores",
    "Department Stores",
    "Gas Stations",
    "Grocery",
    "Parks",
    "Restaurants",
    "Sporting Goods",
  ]

  for (l of locs) {
    for (d of descs) {
      const href = `https://www.yelp.ca/search?find_desc=${encodeURIComponent(d)}&find_loc=${encodeURIComponent(l)}`
      await yelp.scrapeIndex(href, store)
    }
  }
}

main()

/* client.close()
 * })*/
