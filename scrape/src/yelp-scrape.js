const scrape = require('./scrape')
const URL = require('url')
const querystring = require('querystring')

const scrapeYelpBiz = ($) => {
  const result = {}
  result.name = $('.biz-page-title').text().trim()

  result.categories = $('.biz-page-header-left .category-str-list').first().text().split(',').map(x => x.trim())

  const dollarSigns = $('.biz-page-header-left .business-attribute.price-range').text()
  if (dollarSigns) {
    result.priceRange = dollarSigns.length
  }

  result.address = $('.mapbox address').first().text().trim()

  result.phone = $('.mapbox .biz-phone').text().trim()

  result.website =
    querystring.parse(
      URL.parse(
        decodeURIComponent($('.mapbox .biz-website a').attr('href'))
      ).query).url

  result.more = {}
  const x = $('.ywidget h3:contains("More business info")')
    .next()
    .find('.short-def-list')
    .children()
    .each((i,e) => {
      const field = $(e).find('dt').text().trim().replace(/-/g, '').replace(/[\s+]/g,'_').toLowerCase()
      const value = $(e).find('dd').text().trim()
      result.more[field] = value
    })

  return result
}

const scrapeYelpResults = $ => (
  $('li.regular-search-result a.biz-name').map((i, el) => $(el).attr('href').trim()).get()
)

const scrapeIndex = async(href, store) => {
  const qoramp = href.match('/\?/') ? '&' : '?'

  for (let start = 0; ; start += 10) {
    if (start >= 50) break
    const hits = await scrape(`${href}${qoramp}start=${start}`, scrapeYelpResults)
    if (hits.length === 0) break

    for (path of hits) {
      const trimmed = path.replace(/\?.+$/,'')
      const url = `https://www.yelp.ca${trimmed}`
      const data = await scrape(url, scrapeYelpBiz)
      data.url = url
      data.vendor = 'yelp'
      await store(data)
    }
  }
}

module.exports = {
  scrapeIndex
}
