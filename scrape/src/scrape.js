const cheerio = require('cheerio')
const rp = require('request-promise')
const fs = require('fs')

const fetch = async (url) => {
  let html;
  const cacheDir = './cache'
  const filename = `${cacheDir}/${encodeURIComponent(url)}`

  if (fs.existsSync(filename)) {
    html = fs.readFileSync(filename, { encoding: 'utf8' })
  }

  if (html) {
    console.log('HIT', url)
  } else {
    console.log('MISS', url)
    html = await rp(url)
    if (!fs.existsSync(cacheDir)){
      fs.mkdirSync(cacheDir)
    }
    fs.writeFileSync(filename, html)
  }

  return html
}

const scrape = async (url, fn) => {
  const html = await fetch(url)
  return await fn(cheerio.load(html));
}

module.exports = scrape;
