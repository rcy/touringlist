const scrape = require('./scrape')
const URL = require('url')
const querystring = require('querystring')

const scrapeResults = $ => (
  $('.listing a.property_title').map((i, el) => $(el).attr('href').trim()).get()
)

const scrapeHotel = $ => {
  const result = {}

  result.name = $('#HEADING').text().trim()
  result.street_address = $('.street-address').first().text().trim()
  result.locality = $('.locality').first().text().trim().replace(/,$/,'')
  result.country = $('.country-name').first().text().trim()

  result.phone = $('.phone').first().text().trim()

  // amenities

  return result
}

const storeHotels = async (href, store) => {
  const { protocol, host } = URL.parse(href);
  const base = `${protocol}//${host}`

  const hits = await scrape(href, scrapeResults)
  for (path of hits) {
    const url = `${base}${path}`
    console.log(url)
    const data = await scrape(url, scrapeHotel)
    store({ vendor: "tripadvisor", url, data })
  }
}

module.exports = {
  storeHotels
}
